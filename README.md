# 21q3 Tasks

## List of available tasks:
- The first `intro` task can be found on the [Wiki](/%2E%2E/wikis/home) page.
- **Weather** task can be found in the `Weather` subfolder.
- **Timer** is located in the `Timer` subfolder.
- **HLS Service** task.
