#ifndef TIMER_H
#define TIMER_H

#include "logger.h"
#include "timestamps_model.h"
#include <QObject>
#include <QtMultimedia/QSound>

class Timer : public QObject{
    Q_OBJECT
public:
    explicit Timer(QObject *parent = nullptr);
    void setInitialTime(const QString& time);
    void setAlarmSound(const QString& pathToSoundFile);
    void start();
    void pause();
    void reset();
    void tap();
    bool isActive();
private:
    QString mCurrentTime;
    QSound mAlarmSound;
    Logger mLogger;
    TimestampsModel mTimestampsModel;
};

#endif // TIMER_H
